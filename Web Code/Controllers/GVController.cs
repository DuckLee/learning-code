﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Web_Code.Models;

namespace Web_Code.Controllers
{
    public class GVController : Controller
    {
        // GET: GV
        public ActionResult DanhSach()
        {
            Web_CodeEntities1 db = new Web_CodeEntities1();
            List<GIAOVIEN>danhsachGiaoVien = db.GIAOVIENs.ToList();
            return View(danhsachGiaoVien);
        }
        public ActionResult Insert() 
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert(GIAOVIEN model) 
        {
            Web_CodeEntities1 db = new Web_CodeEntities1();
            db.GIAOVIENs.Add(model);
            db.SaveChanges();

            return RedirectToAction("DanhSach");
        }
        public ActionResult Edit(int MAGV) 
        {
            Web_CodeEntities1 db = new Web_CodeEntities1();
            GIAOVIEN model = db.GIAOVIENs.Find(MAGV); 

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(GIAOVIEN model) 
        {
            Web_CodeEntities1 db = new Web_CodeEntities1();
            var UpdateModel = db.GIAOVIENs.Find(model.MAGV);

            UpdateModel.MAGV = model.MAGV;
            UpdateModel.HOTEN = model.HOTEN;
            UpdateModel.NGAYSINH = model.NGAYSINH;
            UpdateModel.DIACHI = model.DIACHI;
            UpdateModel.SDT = model.SDT;
            UpdateModel.MONHOC = model.MONHOC;

            db.SaveChanges();

            return RedirectToAction("DanhSach");
        }
        public ActionResult Delete(int MAGV) 
        {
            Web_CodeEntities1 db = new Web_CodeEntities1();
            var UpdateModel = db.GIAOVIENs.Find(MAGV);
            db.GIAOVIENs.Remove(UpdateModel);

            db.SaveChanges();
            return RedirectToAction("DanhSach");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Code.Models;
namespace Web_Code.Controllers
{
    public class KhoaController : Controller
    {
        // GET: Khoa
        public ActionResult DanhSach()
        {
            return View(DanhSachKhoa.DanhsachKhoa);
        }
        public ActionResult ThemMoi()
        {
            return View(new Khoa() { MAGV = "TK001" });
        }
        [HttpPost]
        public ActionResult ThemMoi(Khoa model)
        {
            if (model.MAGV == null)
            {
                ModelState.AddModelError("MAGV", "Không được để trống");
                return View();
            }
            DanhSachKhoa.DanhsachKhoa.Add(model);
            return RedirectToAction("DanhSach");
        }
        public ActionResult Update()
        {
            var khoa = DanhSachKhoa.DanhsachKhoa.Find(x => x.MAGV == "TK001");
            return View(khoa);
        }
        [HttpPost]
        public ActionResult Update(Khoa model)
        {
            if(model.TENKHOA == "") 
            { 
                ModelState.AddModelError("TENKHOA", "Không được để trống");
                return View();
            }
            var khoa = DanhSachKhoa.DanhsachKhoa.Find(x => x.MAGV == model.MAGV);
            khoa.TENKHOA = model.TENKHOA;
            khoa.MONHOC = model.MONHOC;
            khoa.MABM = model.MABM;
            khoa.TRUONGKHOA = model.TRUONGKHOA;
            khoa.MAGV = model.MAGV;
            khoa.SDT = model.SDT;
            return RedirectToAction("DanhSach");
        }
        public ActionResult Delete() 
        {
         var khoa = DanhSachKhoa.DanhsachKhoa.Find(x => x.MAGV == "TK001");
            DanhSachKhoa.DanhsachKhoa.Remove(khoa);
            return RedirectToAction("DanhSach");
        }
    }
}
  
   